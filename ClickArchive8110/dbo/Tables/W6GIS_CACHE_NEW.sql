﻿CREATE TABLE [dbo].[W6GIS_CACHE_NEW] (
    [Origin_Lat]       INT     NOT NULL,
    [Origin_Long]      INT     NOT NULL,
    [Destination_Lat]  INT     NOT NULL,
    [Destination_Long] INT     NOT NULL,
    [TravelTime]       INT     NULL,
    [Distance]         INT     NULL,
    [GISDataSource]    INT     NULL,
    [UpdateStatus]     INT     NULL,
    [Row_ID]           INT     NOT NULL,
    [Is_Done]          TINYINT NULL
);

