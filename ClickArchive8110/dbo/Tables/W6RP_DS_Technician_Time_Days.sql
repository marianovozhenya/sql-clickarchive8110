﻿CREATE TABLE [dbo].[W6RP_DS_Technician_Time_Days] (
    [EngineerKey]             INT             NOT NULL,
    [Name]                    NVARCHAR (255)  NULL,
    [WorkDate]                VARCHAR (30)    NULL,
    [Available Duration]      INT             NULL,
    [Optional Duration]       INT             NULL,
    [Actual Travel Duration]  INT             NULL,
    [NA Duration]             INT             NOT NULL,
    [Work Duration]           INT             NULL,
    [Default Travel Time]     INT             NULL,
    [Default Travel Distance] DECIMAL (38, 2) NULL
);

