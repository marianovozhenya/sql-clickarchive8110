﻿CREATE TABLE [dbo].[W6RP_DS_Assignemnts] (
    [EngineerKey]           INT             NOT NULL,
    [Name]                  NVARCHAR (255)  NULL,
    [StartTime]             DATETIME        NULL,
    [FinishTime]            DATETIME        NULL,
    [CommentText]           NVARCHAR (255)  NULL,
    [NA Reason]             NVARCHAR (64)   NULL,
    [DOW]                   INT             NULL,
    [Duration]              INT             NULL,
    [CallID]                NVARCHAR (64)   NULL,
    [DefaultTravelTime]     INT             NULL,
    [DefaultTravelDistance] DECIMAL (18, 2) NULL,
    [AssignmentKey]         INT             NOT NULL
);

